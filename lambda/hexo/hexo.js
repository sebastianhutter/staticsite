'use strict';

/* 
    Lambda@Edge for hexo behaviour

    New pages in hexo are always stored like :lang/:page/index.html
    Cloudfront only allows a index.html in the root of the s3 bucket.
    This means we need to ensure that all requests are pointing to an index.html
*/ 

exports.handler = (event, context, callback) => {
    const request = event.Records[0].cf.request;
    // only redirect requests if the request uri is / oder /index.html
    // we need to do this or else css files, images etc cant be loaded properly

    if (request.uri.endsWith('/index.html')) {
        console.log("Request ends with index.html. Doing nothing");
    } else {
        console.log("Request is not ending with index.html. Attach index.html to uri");
        if (request.uri.endsWith('/')) {
            request.uri = request.uri + "index.html"
        } else {
            request.uri = request.uri + "/index.html"
        }
    }
    callback(null, request);
 };
