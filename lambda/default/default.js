'use strict';

/* 
    Lambda@Edge for default cache behaviour

    This lambda@edge fires everytime a user access the website
    (and if no other cache behaviour is fired first).

    It checks for the users language and sends the user to the 
    proper starting page

    It tries to evaluate the language with two strategies:
    1. It checks the accept-language header sent by the users browser
    2. It checks the users country of origin
*/ 

// initialize the accept language parser
var parser = require('accept-language-parser');

// our supported languages
const supportedLanugages = [ 'en', 'de' ];

// the default language if nothing is specified
const defaultLanugage = 'de'

// the homepage for the different lanugages
const homePage = '/home/index.html'

// the following viewer countries will be forwarded
// to the german language pages
const germanLanguageForViewerCountry = [
    'AT',
    'DE',
    'CH'
]

exports.handler = (event, context, callback) => {
    const request = event.Records[0].cf.request;
    const headers = request.headers;

    // only redirect requests if the request uri is / oder /index.html
    // we need to do this or else css files, images etc cant be loaded properly

    if (request.uri == "/" || request.uri == "/index.html") {
        // first get the accepted languages and the viewer country from the headers
        console.log("Retrieve accepted language from request");
        if (headers['accept-language']) {
            var parsedLanguages = parser.parse(headers['accept-language'][0]['value']);
            var accetedLanguages = [];
            parsedLanguages.forEach(l => {
                accetedLanguages.push(l['code'])
            });
        }
        console.log("Retrieve viewer country")
        if (headers['cloudfront-viewer-country']) {
            var viewerCountry = headers['cloudfront-viewer-country'][0]['value'].toLowerCase();
        }
        
        request.uri = "/" + defaultLanugage + homePage;
        if (accetedLanguages) {
            console.log("Accepted Lanugages are set. Try to redirect user via accept-languages: " + accetedLanguages);
            //https://stackoverflow.com/questions/2641347/short-circuit-array-foreach-like-calling-break
            for (let l of accetedLanguages) {
                if (supportedLanugages.includes(l)) {
                    console.log("accepted-languages match supported languegs: " + l);
                    request.uri = "/" + l + homePage;
                    break;
                }
            }
            //request.uri = desktopPath + request.uri;
        } else if (viewerCountry) {
            console.log("Viewer Country is set. Try to redirect user via cloudfront-viewer-countrt: " + viewerCountry)
            if (germanLanguageForViewerCountry.includes(viewerCountry)) {
                console.log("viewer country is in german speaking list.");
                request.uri = "/de" + homePage;
            } else {
                console.log("viewer country is not in german speaking list");
                request.uri = "/en" + homePage;
            }
        }
        console.log(`Request uri set to "${request.uri}"`);
    } else {
        console.log("Request is not / or /index.html. Doing nothing.")
    }
    callback(null, request);
 };
