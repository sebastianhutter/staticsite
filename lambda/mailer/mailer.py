#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import urllib

"""
    send email with contact form content
"""

def response(message, status_code, base64_encoded=False):
    """ send response to api gateway """

    # depending on the status_code we set the success flag
    # in the message
    message = {'message': message }
    if status_code != 200:
        message.update({'success':False})
    else:
        message.update({'success':True})

    return {
        'isBase64Encoded': base64_encoded,
        'statusCode': str(status_code),
        'body': json.dumps(message),
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
    }

def handler(event, context):
    """
        lambda handler
    """

    try:
        # get data from form (formdata is received as query string)
        formdata = urllib.parse.parse_qs(event['body'])
        print('Received form data: {}'.format(formdata))

        if not formdata:
            print('No valid form data received. Aborting')
            return response('Invalid data received', 422)
        
        # check if name, email and message is set 
        if 'name' not in formdata or 'email' not in formdata or 'message' not in formdata:
            print('Incomplete formdata received. Aborting')
            return response('Incomplete data received', 422)

        # 
        # SES code goes here....
        #

        return response('E-Mail scheduled for delivery', 200)
    except Exception as e:
        print("Unhandled exception: {}".format(e))
        return response({'Invalid request'}, 400)

    