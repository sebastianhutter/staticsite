# staticsite

example static site hosted with AWS services

## requirements
- AWS Account which can deploy a multitude of different services
- Route53 DNS Domain (the site is hosted as a subdomain in the domain)
- Docker

## preparation
Copy .env.example to .env and replace the environment variables with your values
