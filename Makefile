##
# VARIABLES
##

DOCKER=docker run --rm -ti --env-file ${CURDIR}/.env -v ${CURDIR}:/data
NPM=${DOCKER} -w /data/content --entrypoint=/usr/local/bin/npm hexo
HEXO=${DOCKER} -p 4000:4000 hexo
ANSIBLE-PLAYBOOK=${DOCKER} -w /data/ansible --entrypoint=/venv/bin/ansible-playbook ansible
LAMBDA-DEFAULT=docker run --rm -i -e DOCKER_LAMBDA_USE_STDIN=1 -v ${CURDIR}/lambda/default:/var/task lambci/lambda:nodejs8.10 default.handler
LAMBDA-HEXO=docker run --rm -i -e DOCKER_LAMBDA_USE_STDIN=1 -v ${CURDIR}/lambda/hexo:/var/task lambci/lambda:nodejs8.10 hexo.handler
LAMBDA-MAILER=docker run --rm -i -e DOCKER_LAMBDA_USE_STDIN=1 -v ${CURDIR}/lambda/mailer:/var/task lambci/lambda:python3.7 mailer.handler
##
# GENERAL TARGETS
##
all: build cloudformation content

# render blog content and sync it
content: blog-render blog-upload

clean: 
	rm -f cloudformation/*.cloudformation
	rm -f lambda/*.zip

##
# BUILD IMAGE TARGETS
##
# build images
build: build-hexo build-ansible

build-hexo:
	docker build -t hexo build/hexo

build-ansible:
	docker build -t ansible build/ansible

##
# CLOUDFORMATION TARGETS
##
cloudformation: cf-acm cf-s3 cf-api-gateway cf-lambda cf-api-definition cf-lambda-edge cf-cloudfront

cf-acm:
	${ANSIBLE-PLAYBOOK} resources.yml -t acm

cf-s3:
	${ANSIBLE-PLAYBOOK} resources.yml -t s3

cf-api-gateway:
	${ANSIBLE-PLAYBOOK} resources.yml -t api-gateway

cf-lambda: lambda-mailer-test
	${ANSIBLE-PLAYBOOK} resources.yml -t lambda

cf-api-definition:
	${ANSIBLE-PLAYBOOK} resources.yml -t api-definition

cf-lambda-edge: lambda-default-test lambda-hexo-test
	${ANSIBLE-PLAYBOOK} resources.yml -t lambda-edge

cf-cloudfront:
	${ANSIBLE-PLAYBOOK} resources.yml -t cloudfront

##
# HEXO BLOG TARGETS
##

# blog setup is only required in a new repository
blog-setup: blog-init blog-theme blog-npm

blog-init: | ./content
	${HEXO} init /data/content

blog-theme: 
	${DOCKER} --entrypoint=/usr/bin/git hexo \
		clone https://github.com/ppoffice/hexo-theme-minos.git \
		content/themes/minos

blog-npm:
	${NPM} install
	${NPM} install hexo-renderer-sass

# render the static pages
blog-render:
	${HEXO} --cwd /data/content generate

# serve the website with the the development webserver
blog-serve: 
	${HEXO} --cwd /data/content server --draft

# upload files to s3
blog-upload: 
	${ANSIBLE-PLAYBOOK} sync.yml

##
# LAMBDA TEST TARGETS
##
lambda-default-test:
	cat "${CURDIR}/lambda/default/events/event-1.json" | ${LAMBDA-DEFAULT}
	cat "${CURDIR}/lambda/default/events/event-2.json" | ${LAMBDA-DEFAULT}
	cat "${CURDIR}/lambda/default/events/event-3.json" | ${LAMBDA-DEFAULT}
	cat "${CURDIR}/lambda/default/events/event-4.json" | ${LAMBDA-DEFAULT}

lambda-hexo-test:
	cat "${CURDIR}/lambda/hexo/events/event-1.json" | ${LAMBDA-HEXO}
	cat "${CURDIR}/lambda/hexo/events/event-2.json" | ${LAMBDA-HEXO}

lambda-mailer-test:
	cat "${CURDIR}/lambda/mailer/events/event-1.json" | ${LAMBDA-MAILER}
	cat "${CURDIR}/lambda/mailer/events/event-2.json" | ${LAMBDA-MAILER}
	cat "${CURDIR}/lambda/mailer/events/event-3.json" | ${LAMBDA-MAILER}
	cat "${CURDIR}/lambda/mailer/events/event-4.json" | ${LAMBDA-MAILER}
	cat "${CURDIR}/lambda/mailer/events/event-5.json" | ${LAMBDA-MAILER}