---
title: REDIRECT
redirect: "5;/de/home"
---

Es scheint als wenn unsere automatische Spracherkennung fehlgeschlagen ist und du deswegen auf der Startseite ohne bestimmte Sprache gelandet bist. Du wirst gleich auf die deutschsprachige Startseite weitergeleitet.

It seems that our automatic language detection and failed and your browser loaded a homepage without a defined language. You will be redirected to the german homepage in any moment.