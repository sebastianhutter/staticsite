---
title: Kontakt
---
Kontaktformular

<form action="https://api-devsite.hutter.cloud/sendmail/" method="POST">
    <ul>
        <li>Name: <input type="text" name="name"></li>
        <li>Email: <input type="text" name="email"></li>
        <li>Nachricht: <textarea name="message"></textarea></li>
    </ul>
    <button type="submit">Absenden</button>
</form>